#!/bin/bash
set -euo pipefail

# Intercept the Ctrl-C signal to be able to stop the call to buildah
trap ctrl_c INT

# Make a note of the package versions.
buildah --version

function say { echo "$@" | toilet -f mono12 -w 400 | lolcat -f || true; }
function echo_green { echo -e "\e[1;32m${1}\e[0m"; }
function echo_red { echo -e "\e[1;31m${1}\e[0m"; }
function echo_yellow { echo -e "\e[1;33m${1}\e[0m"; }

declare -i FAILED=0
declare child_pid

function ctrl_c {
    kill -9 "${child_pid}"
    echo_red "Operation terminated by the user"
    exit 1
}

function loop {
    _loop 5 10 only_notify "$@"
}

function loop_or_fail {
    _loop 5 10 exit_on_fail "$@"
}

function _loop {
    local count=$1 delay=$2 exit_on_fail=$3 s
    shift 3
    for _ in $(seq "${count}"); do
        # we want set -e still in effect, but also get the exit code; so
        # use a background process, as set -e is kept for background
        # commands, and the exit code checking is possible with wait later
        "$@" &
        child_pid=$!
        wait "${child_pid}" && s=0 && break || s=$? && sleep "${delay}"
    done
    FAILED+=$((s > 0))
    if [[ ${exit_on_fail} = "exit_on_fail" ]]; then
        (exit "${s}")
    fi
}

# Build the container.
# shellcheck disable=SC2154
say "${IMAGE_NAME}"

# if running in GitLab, tag with pipeline ID, otherwise latest
if [[ -v CI_PIPELINE_ID ]]; then
    IMAGE_PIPELINE_TAG="p-${PARENT_PIPELINE_ID:-${CI_PIPELINE_ID}}"
else
    IMAGE_PIPELINE_TAG="latest"
fi

# remove any leftover slashes from GitLab regex matching
if [[ -v IMAGE_ARCH ]]; then
    IMAGE_ARCH=${IMAGE_ARCH//\/}
    unset IMAGE_ARCHES
fi

# figure out a reasonable description
IMAGE_DESCRIPTION_PARTS+=("Image: ${IMAGE_NAME}")
if [[ -v CI_PROJECT_URL ]]; then
    IMAGE_DESCRIPTION_PARTS+=("Pipeline: ${CI_PIPELINE_URL}")
    IMAGE_DESCRIPTION_PARTS+=("Job: ${CI_JOB_URL}")
    IMAGE_DESCRIPTION_PARTS+=("Job started at: ${CI_JOB_STARTED_AT}")
    if [[ -v CI_MERGE_REQUEST_PROJECT_URL ]]; then
        IMAGE_DESCRIPTION_PARTS+=("Merge Request: ${CI_MERGE_REQUEST_PROJECT_URL}/-/merge_requests/${CI_MERGE_REQUEST_IID}")
        IMAGE_DESCRIPTION_PARTS+=("Commit: ${CI_MERGE_REQUEST_PROJECT_URL}/-/commit/${CI_COMMIT_SHA}")
    elif [[ -v CI_COMMIT_TAG ]]; then
        IMAGE_DESCRIPTION_PARTS+=("Tag: ${CI_PROJECT_URL}/-/tags/${CI_COMMIT_TAG}")
        IMAGE_DESCRIPTION_PARTS+=("Commit: ${CI_PROJECT_URL}/-/commit/${CI_COMMIT_SHA}")
    elif [[ -v CI_COMMIT_BRANCH ]]; then
        IMAGE_DESCRIPTION_PARTS+=("Branch: ${CI_PROJECT_URL}/-/tree/${CI_COMMIT_BRANCH}")
        IMAGE_DESCRIPTION_PARTS+=("Commit: ${CI_PROJECT_URL}/-/commit/${CI_COMMIT_SHA}")
    else
        IMAGE_DESCRIPTION_PARTS+=("Commit: ${CI_PROJECT_URL}/-/commit/${CI_COMMIT_SHA}")
    fi
    IMAGE_DESCRIPTION_PARTS+=("Commit title: ${CI_COMMIT_TITLE}")
    IMAGE_DESCRIPTION_PARTS+=("Commit timestamp: ${CI_COMMIT_TIMESTAMP}")
fi
IMAGE_DESCRIPTION=$(printf '%s\n' "${IMAGE_DESCRIPTION_PARTS[@]}")

IMAGE_EXTRA_TAGS=()
if [[ -v AUTOMATIC_TAGS ]]; then
    IMAGE_EXTRA_TAGS+=("g-${PARENT_PIPELINE_ID:-${CI_PIPELINE_ID}}")
    if [[ -v CI_MERGE_REQUEST_IID ]]; then
        IMAGE_EXTRA_TAGS+=("mr-${CI_MERGE_REQUEST_IID}")
    fi
    if [[ -v CI_COMMIT_TAG ]]; then
        IMAGE_EXTRA_TAGS+=("${CI_COMMIT_TAG}")
    fi
    # shellcheck disable=SC2154
    if [[ -v CI_COMMIT_REF_NAME ]] && [[ ${CI_COMMIT_REF_NAME} = "${CI_DEFAULT_BRANCH}" ]]; then
        IMAGE_EXTRA_TAGS+=("latest")
    fi
elif [[ -v DEPLOY_TAG ]]; then
    IMAGE_PIPELINE_TAG="g-${PARENT_PIPELINE_ID:-${CI_PIPELINE_ID}}"
    IMAGE_EXTRA_TAGS+=("${DEPLOY_TAG}")
fi

export BUILDAH_CPPFLAGS='-Ibuilds -Iincludes -I/usr/local/share/cki/buildah'

echo_yellow "Buildah build"

# enable qemu support; this works in the buildah image, and should cause no
# harm on a local installation where the script most likely is not installed
if [[ -n ${IMAGE_ARCH:-} ]] && [[ -x /usr/local/bin/qemu-binfmt-conf.sh ]]; then
    if [[ ! -f /proc/sys/fs/binfmt_misc/register ]]; then
        mount binfmt_misc -t binfmt_misc /proc/sys/fs/binfmt_misc || true
    fi
    for format in /proc/sys/fs/binfmt_misc/qemu-*; do echo -1 > "${format}" || true; done
    qemu-binfmt-conf.sh --qemu-suffix -static --qemu-path /usr/bin --persistent yes || true
fi

export IMAGE="${IMAGE_NAME}:${IMAGE_PIPELINE_TAG}${IMAGE_ARCH:+-${IMAGE_ARCH}}"

if [[ -v IMAGE_ARCH ]]; then
    case "${IMAGE_ARCH}" in
        amd64) export KERNEL_IMAGE_ARCH=x86_64;;
        arm64) export KERNEL_IMAGE_ARCH=aarch64;;
        *) export KERNEL_IMAGE_ARCH=${IMAGE_ARCH};;
    esac
fi

if ! [[ -v IMAGE_ARCHES ]] && ! [[ -v DEPLOY_TAG ]] && ! [[ -v AUTOMATIC_TAGS ]]; then
    # shellcheck disable=SC2154
    overrides=()
    for var in "${!BUILD_ARG_@}"; do
        overrides+=(--build-arg "${var}=${!var}")
    done
    loop_or_fail buildah bud \
        ${BASE_IMAGE+--cpp-flag "-D_BASE_IMAGE=${BASE_IMAGE}"} \
        ${BASE_IMAGE_TAG+--cpp-flag "-D_BASE_IMAGE_TAG=${BASE_IMAGE_TAG}"} \
        ${IMAGE_NAME+--cpp-flag "-D_IMAGE_NAME=cki-project/${IMAGE_NAME}"} \
        ${IMAGE_DESCRIPTION+--build-arg "IMAGE_DESCRIPTION=${IMAGE_DESCRIPTION}"} \
        ${IMAGE_ARCH+--cpp-flag "-D_IMAGE_ARCH=${IMAGE_ARCH}"} \
        ${KERNEL_IMAGE_ARCH+--cpp-flag "-D_KERNEL_IMAGE_ARCH=${KERNEL_IMAGE_ARCH}"} \
        ${IMAGE_ARCH:+--arch "${IMAGE_ARCH}"} \
        ${IMAGE_ARCH:+--override-arch "${IMAGE_ARCH}"} \
        "${overrides[@]}" \
        --omit-history \
        --file "builds/${IMAGE_NAME}".in \
        --tag "${IMAGE}" \
        .
fi

# if not running in GitLab, do not push
if ! [[ -v CI_PIPELINE_ID ]]; then
    exit 0
fi

if [[ -v REGISTRY ]]; then
    REGISTRIES=("${REGISTRY}")
elif [[ -v REGISTRIES ]]; then
    read -r -a REGISTRIES <<< "${REGISTRIES}"
else
    REGISTRIES=(CI_REGISTRY QUAY_IO DOCKER_IO)
fi

export REGISTRY_AUTH_FILE=${HOME}/auth.json
for REGISTRY_VAR in "${REGISTRIES[@]}"; do
    if ! [[ -v "${REGISTRY_VAR}" ]]; then
        continue
    fi
    REGISTRY_PASSWORD_VAR="${REGISTRY_VAR}_PASSWORD"
    REGISTRY_USER_VAR="${REGISTRY_VAR}_USER"
    REGISTRY_IMAGE_VAR="${REGISTRY_VAR}_IMAGE"
    if ! [[ -v "${REGISTRY_PASSWORD_VAR}" ]] || \
       ! [[ -v "${REGISTRY_USER_VAR}" ]] || \
       ! [[ -v "${REGISTRY_IMAGE_VAR}" ]]; then
        continue
    fi
    loop skopeo login -u "${!REGISTRY_USER_VAR}" -p "${!REGISTRY_PASSWORD_VAR}" "${!REGISTRY_VAR}"
    REGISTRY_TAG=${!REGISTRY_IMAGE_VAR}/${IMAGE}

    if [[ -v DEPLOY_TAG ]] || [[ -v AUTOMATIC_TAGS ]]; then
        for EXTRA_TAG in "${IMAGE_EXTRA_TAGS[@]}"; do
            EXTRA_REGISTRY_TAG="${REGISTRY_TAG%:*}:${EXTRA_TAG}${IMAGE_ARCH:+-${IMAGE_ARCH}}"
            echo_yellow "tagging ${REGISTRY_TAG} as ${EXTRA_REGISTRY_TAG}"
            loop skopeo copy --all "docker://${REGISTRY_TAG}" "docker://${EXTRA_REGISTRY_TAG}"
        done
    elif [[ -v IMAGE_ARCHES ]]; then
        IFS=' ' read -r -a IMAGE_ARCHES_ARRAY <<< "${IMAGE_ARCHES}"
        # buildah manifest create cannot pull from private repos
        for IMAGE_ARCH in "${IMAGE_ARCHES_ARRAY[@]}"; do
            echo_yellow "pulling ${REGISTRY_TAG}-${IMAGE_ARCH}"
            loop buildah pull "docker://${REGISTRY_TAG}-${IMAGE_ARCH}"
        done
        buildah manifest create "${REGISTRY_TAG}" "${IMAGE_ARCHES_ARRAY[@]/#/containers-storage:${REGISTRY_TAG}-}"
        echo_yellow "pushing ${REGISTRY_TAG}"
        loop buildah manifest push --all "${REGISTRY_TAG}" "docker://${REGISTRY_TAG}"
    else
        echo_yellow "pushing to ${REGISTRY_TAG}"
        loop buildah push "${IMAGE}" "docker://${REGISTRY_TAG}"
    fi
done

if (( FAILED > 0 )); then
    echo_red "${FAILED} registry pushes failed."
    exit 1
fi
